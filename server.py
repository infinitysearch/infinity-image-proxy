from flask import Flask, render_template, request, redirect, send_from_directory, send_file
import requests
import io
import os

app = Flask(__name__)


@app.route('/')
def render_home():
    return render_template('home.html')


@app.route('/image')
def render_external_image():
    if request.args.get('url') is None:
        return ''
    try:
        image = io.BytesIO(requests.get(request.args.get('url')).content)
        return send_file(image, attachment_filename='external_image.png', mimetype='image/png')
    except Exception:
        return ''


@app.before_request
def before_request():
    # This is just so that we do not force https when testing the site on localhost
    if request.url.startswith('http://localhost') or request.url.startswith(
            'http://127.0.0.1'):  # If it is being ran locally
        return

    # Auto redirect to https
    if request.url.startswith('http://'):
        url = request.url.replace('http://', 'https://', 1)
        code = 301
        return redirect(url, code=code)

    return


@app.after_request
def add_header(response):
    if request.url.startswith('http://localhost') or request.url.startswith(
            'http://127.0.0.1'):  # If it is being ran locally
        response.headers['Strict-Transport-Security'] = 'max-age=63072000; includeSubDomains; preload'
        response.headers['X-Content-Type-Options'] = 'nosniff'
        response.headers['Content-Security-Policy'] = "default-src https; frame-ancestors 'none'"
        response.headers['Content-Security-Policy'] = "frame-ancestors 'none'"
        response.headers['X-Frame-Options'] = 'DENY'
        response.headers['X-XSS-Protection'] = '1; mode=block'
        return response

    response.headers['Strict-Transport-Security'] = 'max-age=63072000; includeSubDomains; preload'
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers['Content-Security-Policy'] = "default-src https; frame-ancestors 'none'"
    response.headers['Content-Security-Policy'] = "frame-ancestors 'none'"
    response.headers['X-Frame-Options'] = 'DENY'
    response.headers['X-XSS-Protection'] = '1; mode=block'

    return response


@app.route('/robots.txt')
def render_robots():
    return send_from_directory(app.static_folder, 'robots.txt')


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.png',
                               mimetype='image/vnd.microsoft.icon')


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    return ''


if __name__ == '__main__':
    app.run()
