# Infinity Image Proxy Development

We use this in Infinity Search to display our images. 

The only code that really does anything for us is this part below in [server.py](/server.py):
```python
@app.route('/external')
def render_external_image():
    if request.args.get('url') is None:
        return ''
    try:
        image = io.BytesIO(requests.get(request.args.get('url')).content)
        return send_file(image, attachment_filename='external_image.png', mimetype='image/png')
    except Exception:
        return ''

```
